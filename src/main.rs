mod Ondas;
mod Operaciones;
use Ondas::Campo_Electro_Magnetico_Escalar as C_E_E;
use std::*;
use Operaciones::x_10_pow_Al as x_10;
fn main() {
    println!(" Problema 1!!!!\n\n\nUna carga de 5×10^-6 C se
    introduce a una región donde actúa un campo
    de fuerza de 0.04N. ¿Cuál es la intensidad del
    campo eléctrico en esa región?\n");
    let mut campo_1 = C_E_E::new();
    print!("Es: {} N/C \n\n\n", campo_1.Inten_Camp_Ele_F_y_q( 0.04f32, 5f32*10f32.powf(-6f32) ) );


    println!("Problema 2!!!!\n\n\nDada la imagen, y asumiendo
    que se coloca una carga q = 2×10^-7 C, y en
    ella actúa una fuerza F= 5×10^-2N, ¿Cuál es
    entonces, la intensidad del campo en P?\n");
    let mut campo_2= C_E_E::new();
    print!("Es: {} N/C \n\n\n", campo_2.Inten_Camp_Ele_F_y_q( x_10(5f32,-2f32), x_10(2f32,-7f32) ) );

    println!("Problema 3!!!!\n\n\n¿Cuál es el valor de la carga
    que está sometida a un campo eléctrico de
    4.5×10^5 N/C  y sobre ella se aplica una
    fuerza de 8.6 x10^-2 N?\n");
    let mut campo_3 = C_E_E::new();
    print!("Es: {} C \n\n\n", campo_3.Carg_E_F( x_10(8.6,-2f32), x_10(4.5,5f32) ) );

    println!("Problema 4!!!!\n\n\nCalcule la magnitud de la
    intensidad del campo eléctrico a una distancia
    de 75 cm de una carga de 3μC\n");
    let mut campo_4 = C_E_E::new();
    let d = campo_4.dist_from_cm(75f32);
    print!("Es: {} C \n\n\n", campo_4.Inten_Camp_dist( x_10(3f32,-6f32) , d ) );



}
