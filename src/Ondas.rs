use std::*;
mod Operaciones;
use Operaciones::x_10_pow_Al as x_10;
use std::f32::consts::PI as PI;


pub const K: f32 = 1f32/(4f32*PI*ep0);

pub const ep0: f32 =  0.000000000008854187817;

pub fn K_cons() -> f32{
    1f32/(4f32*PI*ep0)
}



pub struct Onda {
    vel: f32, 
    lamb: f32, 
    frec: f32 , 
    c_frec: f32,
    per: f32 ,
    em_vel: f32,
}
impl Onda {
    pub fn new() -> Onda {
        Onda {
            vel : 0f32, lamb : 0f32, frec: 0f32 ,
            per: 0f32, em_vel: 0f32, c_frec: 0f32,
         }
    }
}
pub struct Campo_Electro_Magnetico_Escalar {
    E: f32,
    F: f32,
    q: f32,
    r: f32,
    V: f32,
    Ep: f32,
    Ec: f32,
    W: f32,
    u: f32,
    d: f32
}
pub struct Campo_Electro_Magnetico_Vectorial {
    E: Vec<f32>,
    F: Vec<f32>,
    q: f32,
    r: Vec<f32>,
    V: f32,
    Ep: Vec<f32>,
    Ec: Vec<f32>,
    W: Vec<f32>,
    d: Vec<f32>,
    u: Vec<f32>
}

impl Campo_Electro_Magnetico_Escalar {
    pub fn new() -> Campo_Electro_Magnetico_Escalar {
        Campo_Electro_Magnetico_Escalar {
            E: 0f32,
            F: 0f32,
            q: 0f32,
            r: 0f32,
            V: 0f32,
            Ep: 0f32,
            Ec: 0f32,
            W: 0f32,
            d: 0f32,
            u: 0f32 
        }
    }
    pub fn Inten_Camp_Ele_F_y_q(&mut self,F: f32, q: f32) -> f32 {
        self.q = q;
        self.F = F;
        self.E = F/q;
        self.E
    }
    pub fn Inten_Camp_Ele_F_y_q_IV(&mut self) -> f32 {
        self.E = self.F/self.q;
        self.E
    }
    pub fn Carg_E_F(&mut self, F: f32, E: f32) -> f32 {
        self.F = F;
        self.E = E;
        self.q = F/E;
        self.q
    }
    pub fn Carg_E_F_IV(&mut self) -> f32 {
        self.q = self.F/self.E;
        self.E
    }
    pub fn dist_from_cm(&mut self, d: f32) -> f32 {
        self.d = d /100f32; 
        self.d
    }
    pub fn Inten_Camp_dist(&mut self, q: f32, d: f32) -> f32 {
        self.q = q;
        self.d = d;
        self.E = K * ( self.q / self.d.powf(2f32) );
        K * ( self.q / self.d.powf(2f32) )
    }

}